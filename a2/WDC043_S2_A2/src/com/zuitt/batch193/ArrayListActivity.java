package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Arrays;


public class ArrayListActivity {
    public static void main (String[] args){
        ArrayList<String> myFriends = new ArrayList<>(Arrays.asList("Luffy", "Sanji", "Robin","Nami", "Chopper"));

        System.out.println("My friends are: " + myFriends);
    }
}

