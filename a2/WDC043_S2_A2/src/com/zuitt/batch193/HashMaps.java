package com.zuitt.batch193;

import java.util.HashMap;

public class HashMaps {
    public static void main(String[] args) {
        HashMap <String, Integer> inventory = new HashMap<>();
        inventory.put("Nike Quest 4", 3890);
        inventory.put("Honda CBR 150", 162_000);
        inventory.put("GTX 3080", 75_000);

        System.out.println(inventory);
    }
}
